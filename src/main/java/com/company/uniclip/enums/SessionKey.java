package com.company.uniclip.enums;

public enum  SessionKey {
    GOOGLE_OAUTH_TOKEN,
    FACEBOOK_OAUTH_TOKEN
}
