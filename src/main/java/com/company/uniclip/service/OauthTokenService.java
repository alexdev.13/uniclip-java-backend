package com.company.uniclip.service;

import com.company.uniclip.dto.OauthResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

@Service
public class OauthTokenService {

    @Value("${spring.security.oauth2.client.registration.google.client-id}")
    private String CLIENT_GOOGLE_ID;
    @Value("${spring.security.oauth2.client.registration.google.client-secret}")
    private String CLIENT_GOOGLE_SECRET;

    @Value("${spring.security.oauth2.client.registration.facebook.client-id}")
    private String CLIENT_FACEBOOK_ID;
    @Value("${spring.security.oauth2.client.registration.facebook.client-secret}")
    private String CLIENT_FACEBOOK_SECRET;

    private final RestTemplate restTemplate;

    public OauthTokenService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String fetchGoogleToken(String code, String scope) {
        final String uri = "https://accounts.google.com/o/oauth2/token";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String clientCredentials = Base64.getEncoder().encodeToString((CLIENT_GOOGLE_ID +":"+ CLIENT_GOOGLE_SECRET).getBytes());
        headers.add("Authorization", "Basic "+clientCredentials);
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("code", code);
        requestBody.add("grant_type", "authorization_code");
        requestBody.add("redirect_uri", "https://uniclip.fun:9000/oauth2/callback/google");
        requestBody.add("scope", scope);

        HttpEntity formEntity = new HttpEntity<>(requestBody, headers);

        ResponseEntity<OauthResponse> response = restTemplate.exchange(uri, HttpMethod.POST, formEntity,  OauthResponse.class);
        return response.getBody().getAccess_token();
    }

    public String fetchFacebookToken(String code) {
        final String uri = "https://graph.facebook.com/oauth/access_token";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String clientCredentials = Base64.getEncoder().encodeToString((CLIENT_FACEBOOK_ID +":"+ CLIENT_FACEBOOK_SECRET).getBytes());
        headers.add("Authorization", "Basic "+clientCredentials);
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("code", code);
        requestBody.add("grant_type", "authorization_code");
        requestBody.add("redirect_uri", "https://localhost:9000/oauth2/callback/facebook");
//        requestBody.add("scope", scope);

        HttpEntity formEntity = new HttpEntity<>(requestBody, headers);

        ResponseEntity<OauthResponse> response = restTemplate.exchange(uri, HttpMethod.POST, formEntity,  OauthResponse.class);
        return response.getBody().toString();
    }
}
