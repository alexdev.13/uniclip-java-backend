package com.company.uniclip.controller;

import com.company.uniclip.enums.SessionKey;
import com.company.uniclip.exception.AccessDeniedException;
import com.company.uniclip.service.OauthTokenService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class FacebookAuthRedirectController {

    private final OauthTokenService oauthTokenService;

    public FacebookAuthRedirectController(OauthTokenService oauthTokenService) {
        this.oauthTokenService = oauthTokenService;
    }

    @RequestMapping("/oauth2/callback/facebook")
    public String callbackUrl(
            HttpServletRequest request,
            HttpSession httpSession) {
        String code = request.getParameter("code");
        String accessDenied = request.getParameter("access_denied") == null
                ? "" : request.getParameter("access_denied");
        if (!accessDenied.isBlank()) throw new AccessDeniedException("Authorization from facebook failed");
        String error = request.getParameter("error") == null
                ? "" : request.getParameter("error");
        if (!error.isBlank()) throw new AccessDeniedException("Authorization from facebook failed");
//        String[] scopes = request.getParameter("scope").split(" ");
//        if (code.isBlank()) throw new AccessDeniedException("Authorization from facebook failed");
//        String permission =
//                Stream.of(scopes)
//                        .filter(s -> s.contains("drive"))
//                        .findFirst()
//                        .orElseThrow(() -> new AccessDeniedException("You must have to allow drive data to be accessed."));
        httpSession
                .setAttribute(SessionKey.FACEBOOK_OAUTH_TOKEN.toString(),
                        oauthTokenService.fetchFacebookToken(code)
                );
        return "redirect:/";
    }
}
