package com.company.uniclip.controller;

import com.company.uniclip.dto.*;
import com.company.uniclip.enums.SessionKey;
import com.company.uniclip.exception.AccessDeniedException;
import com.company.uniclip.service.GoogleDriveService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("/api/v1")
public class DriveController {

    private final GoogleDriveService googleDriveService;

    public DriveController(GoogleDriveService googleDriveService) {
        this.googleDriveService = googleDriveService;
    }

    @GetMapping("/drive")
    public Collection<FileDto> getDrive(HttpSession session) {
        String accessToken = session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()) == null
                ? "" : session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()).toString();

        if (accessToken == null || accessToken.isBlank()) {
            throw new AccessDeniedException("Invalid token");
        }
        DriveFiles driveFiles = googleDriveService.getDriveFiles(accessToken);

        System.out.println(driveFiles.toString());

        return toDriveMappers(driveFiles.getItems());
    }

    @GetMapping("/albums")
    public DriveAlbums getAlbums(HttpSession session) {
        String accessToken = session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()) == null
                ? "" : session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()).toString();

        if (accessToken == null || accessToken.isBlank()) {
            throw new AccessDeniedException("Invalid token");
        }
        DriveAlbums albums = googleDriveService.getPhotoAlbums(accessToken);

        return albums;
    }


    @GetMapping("/photo")
    public PhotoFiles getPhoto(HttpSession session) {
        String accessToken = session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()) == null
                ? "" : session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()).toString();

        if (accessToken == null || accessToken.isBlank()) {
            throw new AccessDeniedException("Invalid token");
        }
        PhotoFiles photoFiles = googleDriveService.getPhotoFiles(accessToken);

        System.out.println(photoFiles.toString());

        return photoFiles;
    }

    @GetMapping("/profile")
    public Profile getProfile(HttpSession session) {
        String accessToken = session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()) == null
                ? "" : session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()).toString();

        if (accessToken == null || accessToken.isBlank()) {
            throw new AccessDeniedException("Invalid token");
        }
        Profile profile = googleDriveService.getProfile(accessToken);

        System.out.println(profile.toString());

        return profile;
    }


    private Collection<FileDto> toDriveMappers(Collection<File> files) {
        Collection<FileDto> fileDtos = new ArrayList<>();

        for (File file : files) {
            fileDtos.add(toDriveMapper(file));
        }
        return fileDtos;
    }



    private FileDto toDriveMapper(File file) {
        FileDto fileDto = new FileDto();
        fileDto.setName(file.getTitle());
        fileDto.setKind(file.getKind());
        fileDto.setMimeType(file.getMimeType());
        fileDto.setId(file.getId());
        fileDto.setAlternateLink(file.getAlternateLink());
        fileDto.setEmbedLink(file.getEmbedLink());
        fileDto.setIconLink(file.getIconLink());
        fileDto.setSelfLink(file.getSelfLink());
        fileDto.setThumbnailLink(file.getThumbnailLink());
        fileDto.setCreatedDate(file.getCreatedDate());
        fileDto.setModifiedDate(file.getModifiedDate());
        fileDto.setFileSize(file.getFileSize());
        return fileDto;
    }

    private Collection<FileDto> toPhotoMappers(Collection<PFile> files) {
        Collection<FileDto> fileDtos = new ArrayList<>();

        for (PFile file : files) {
            fileDtos.add(toPhotoMapper(file));
        }
        return fileDtos;
    }

    private FileDto toPhotoMapper(PFile file) {
        FileDto fileDto = new FileDto();
        fileDto.setName(file.getFilename());
        fileDto.setKind(file.getKind());
        fileDto.setMimeType(file.getMimeType());
        fileDto.setId(file.getId());
        return fileDto;
    }

}
