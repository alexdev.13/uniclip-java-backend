package com.company.uniclip.dto;

import java.io.Serializable;
import java.util.List;

public class DriveFiles implements Serializable {
    private String kind;
    private String nextPageToken;
    private String incompleteSearch;
    private List<File> items;

    public DriveFiles() {
    }

    public DriveFiles(String kind, String nextPageToken, String incompleteSearch, List<File> items) {
        this.kind = kind;
        this.nextPageToken = nextPageToken;
        this.incompleteSearch = incompleteSearch;
        this.items = items;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public String getIncompleteSearch() {
        return incompleteSearch;
    }

    public void setIncompleteSearch(String incompleteSearch) {
        this.incompleteSearch = incompleteSearch;
    }

    public List<File> getItems() {
        return items;
    }

    public void setItems(List<File> items) {
        this.items = items;
    }
}
