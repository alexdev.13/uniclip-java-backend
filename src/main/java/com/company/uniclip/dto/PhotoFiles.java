package com.company.uniclip.dto;


import java.io.Serializable;
import java.util.List;

public class PhotoFiles implements Serializable {
    private String kind;
    private String nextPageToken;
    private String incompleteSearch;
    private List<PFile> mediaItems;

    public PhotoFiles() {
    }

    public PhotoFiles(String kind, String nextPageToken, String incompleteSearch, List<PFile> mediaItems) {
        this.kind = kind;
        this.nextPageToken = nextPageToken;
        this.incompleteSearch = incompleteSearch;
        this.mediaItems = mediaItems;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public String getIncompleteSearch() {
        return incompleteSearch;
    }

    public void setIncompleteSearch(String incompleteSearch) {
        this.incompleteSearch = incompleteSearch;
    }

    public List<PFile> getMediaItems() {
        return mediaItems;
    }

    public void setMediaItems(List<PFile> mediaItems) {
        this.mediaItems = mediaItems;
    }
}
