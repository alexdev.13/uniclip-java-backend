package com.company.uniclip.dto;


import java.io.Serializable;


public class File implements Serializable {
    private String kind;
    private String id;
    private String title;
    private String mimeType;
    private String selfLink;
    private String alternateLink;
    private String embedLink;
    private String iconLink;
    private String thumbnailLink;
    private String createdDate;
    private String modifiedDate;
    private Long fileSize;

    public File(String kind, String id, String title, String mimeType, String selfLink, String alternateLink, String embedLink, String iconLink, String thumbnailLink, String createdDate, String modifiedDate, Long fileSize) {
        this.kind = kind;
        this.id = id;
        this.title = title;
        this.mimeType = mimeType;
        this.selfLink = selfLink;
        this.alternateLink = alternateLink;
        this.embedLink = embedLink;
        this.iconLink = iconLink;
        this.thumbnailLink = thumbnailLink;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.fileSize = fileSize;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    public String getAlternateLink() {
        return alternateLink;
    }

    public void setAlternateLink(String alternateLink) {
        this.alternateLink = alternateLink;
    }

    public String getEmbedLink() {
        return embedLink;
    }

    public void setEmbedLink(String embedLink) {
        this.embedLink = embedLink;
    }

    public String getIconLink() {
        return iconLink;
    }

    public void setIconLink(String iconLink) {
        this.iconLink = iconLink;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public File() {
    }
}
