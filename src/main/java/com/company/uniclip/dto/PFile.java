package com.company.uniclip.dto;

public class PFile {
    private String kind;
    private String id;
    private String mimeType;
    private String filename;
    private String productUrl;
    private String baseUrl;
    private MetaData mediaMetadata;

    public PFile() {
    }

    public PFile(String kind, String id, String mimeType, String filename, String productUrl, String baseUrl, MetaData mediaMetadata) {
        this.kind = kind;
        this.id = id;
        this.mimeType = mimeType;
        this.filename = filename;
        this.productUrl = productUrl;
        this.baseUrl = baseUrl;
        this.mediaMetadata = mediaMetadata;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public MetaData getmediaMetadata() {
        return mediaMetadata;
    }

    public void setmediaMetadata(MetaData mediaMetadata) {
        this.mediaMetadata = mediaMetadata;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }
}
