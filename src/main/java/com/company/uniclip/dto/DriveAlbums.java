package com.company.uniclip.dto;


import java.util.List;

public class DriveAlbums {

    private List<Albums> albums;

    public DriveAlbums() {
    }

    public DriveAlbums(List<Albums> albums) {
        this.albums = albums;
    }

    public List<Albums> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Albums> albums) {
        this.albums = albums;
    }
}
