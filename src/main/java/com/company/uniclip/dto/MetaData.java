package com.company.uniclip.dto;

public class MetaData {

    private String creationTime;
    private String width;
    private String height;

    public MetaData(String creationTime, String width, String height) {
        this.creationTime = creationTime;
        this.width = width;
        this.height = height;
    }

    public MetaData() {
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
